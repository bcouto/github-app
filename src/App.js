import React, { Component } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import getRepository from "./scripts/getRepository";
import Menu from "./components/Menu";
import HomeContent from "./components/HomeContent";
import ProjectContent from "./components/ProjectContent";
import "./styles/App.css";

class App extends Component {
  state = {
    respository: {}
  };

  componentWillMount() {
    this.saveRepository();
  }

  saveRepository() {
    getRepository().then(response => {
      this.setState({
        respository: response
      });
    });
  }

  render() {
    const repository = this.state.respository;

    return (
      <Router>
        <div className="App">
          <Menu repository={repository} />

          <Route exact={true} path="/" component={HomeContent} />

          {repository.length && (
            <Route
              path="/:project"
              render={({ match }) => (
                <ProjectContent
                  project={repository.find(
                    p => p.name === match.params.project
                  )}
                />
              )}
            />
          )}
        </div>
      </Router>
    );
  }
}

// const ProjectContent = ({ project }) => {
//   return <div className="">{project.name}</div>;
// };

export default App;
