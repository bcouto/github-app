import React from "react";
import placeholder from "../images/placeholder.png";

const authorImage = commit => {
  if (commit.author) {
    return commit.author.avatar_url;
  } else {
    return placeholder;
  }
};

const authorNickname = commit => {
  if (commit.author) {
    return "@" + commit.author.login;
  }
};

const authorDate = commit => {
  if (commit.author) {
    return commit.commit.author.date.split("T")[0];
  }
};

const Commit = function(commit, index) {
  if (commit) {
    return (
      <div className="Commit" key={index}>
        <img className="Commit-author-image" src={authorImage(commit)} />
        <div className="Commit-data">
          <p className="Commit-message">{commit.commit.message}</p>
          <span className="Commit-author-nickname">
            {authorNickname(commit)}
          </span>
          <span className="Commit-date">{authorDate(commit)}</span>
        </div>
      </div>
    );
  }
};

export default Commit;
