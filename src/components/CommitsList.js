import React from "react";
import getBranches from "../scripts/getBranches";
import getCommitsInfo from "../scripts/getCommitsInfo";
import Commit from "./Commit";
import LoadCommitsButton from "./LoadCommitsButton";
import loadCommitsButton from "./LoadCommitsButton";

class CommitsLists extends React.Component {
  state = {
    isFetching: false,
    commitsURI: String,
    commits: [],
    commitsRendered: 20
  };

  componentWillMount() {
    this.getBranchesURI(this.props, false);
  }

  componentWillReceiveProps(nextProps) {
    this.getBranchesURI(nextProps, true);
  }

  getBranchesURI = (props, newProject) => {
    this.setState({
      isFetching: true
    });

    getBranches(props.project).then(response => {
      this.createBranchesURI(this.getSHA(response), newProject);
    });
  };

  getSHA(response) {
    return response.find(p => p.name === "master").commit.sha;
  }

  createBranchesURI(SHA, newProject) {
    const URI =
      "https://api.github.com/repos/marvin-ai/" +
      this.props.project.name +
      "/commits?per_page=100&sha=" +
      SHA;
    this.setState({
      commitsURI: URI,
      isFetching: false
    });

    getCommitsInfo(this.state.commitsURI).then(response => {
      let newCommits;

      if (newProject) {
        newCommits = response;
      } else {
        newCommits = this.state.commits.concat(response);
      }

      this.setState({
        commits: newCommits,
        isFetching: false
      });

      if (response.length === 100) {
        this.createBranchesURI(response[99].sha, false);
      }
    });
  }

  getRenderedCommits = addCommits => {
    if (addCommits) {
      if (this.state.commits.length > this.state.commitsRendered + 20) {
        this.setState({
          commitsRendered: this.state.commitsRendered + 20
        });
      } else {
        this.setState({
          commitsRendered: this.state.commits.length
        });
      }
    }

    let renderedCommits = [];

    for (let index = 0; index <= this.state.commitsRendered; index++) {
      renderedCommits[index] = this.state.commits[index];
    }

    renderedCommits = renderedCommits.map((commit, index) =>
      Commit(commit, index)
    );

    return renderedCommits;
  };

  loadButton() {
    if (this.state.commits.length === this.state.commitsRendered) {
      return;
    } else {
      return <LoadCommitsButton getRenderedCommits={this.getRenderedCommits} />;
    }
  }

  render() {
    return (
      <div className="Commit-list">
        {this.loadButton()}
        {this.getRenderedCommits()}
      </div>
    );
  }
}

export default CommitsLists;
