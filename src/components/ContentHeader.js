import React from "react";

const ContentHeader = function(props) {
  return (
    <div className="Content-Header">
      <div className="Project-name">
        <span>Name: </span>
        {props.project.name}
      </div>
      <div className="Project-stars">
        <span>Stars: </span>
        {props.project.stargazers_count}
      </div>
      <div className="Project-forks">
        <span>Forks: </span>
        {props.project.forks_count}
      </div>
    </div>
  );
};
export default ContentHeader;
