import React from "react";
import { Link } from "react-router-dom";
import "../styles/Menu.css";

const repositoryNameList = function(repository) {
  if (repository.length) {
    const orderedRepository = repository.sort((obj1, obj2) => {
      return obj2.stargazers_count - obj1.stargazers_count;
    });

    const renderedNames = orderedRepository.map(repository => {
      return (
        <span key={repository.id} className={"Menu-project-" + repository}>
          <Link to={`/${repository.name}`}>{repository.name}</Link>
        </span>
      );
    });

    return <div className="Menu-items">{renderedNames}</div>;
  }
};

const Menu = function(props) {
  return (
    <div className="App-menu">
      <span className="Menu-title">Projetos</span>
      {repositoryNameList(props.repository)}
    </div>
  );
};

export default Menu;
