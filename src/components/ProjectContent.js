import React from "react";
import "../styles/Content.css";
import "../styles/ProjectContent.css";
import ContentHeader from "./ContentHeader";
import CommitsList from "./CommitsList";

export default function({ project }) {
  return (
    <div className="App-content">
      <ContentHeader project={project} />
      <CommitsList project={project} />
    </div>
  );
}
//https://stackoverflow.com/questions/9179828/github-api-retrieve-all-commits-for-all-branches-for-a-repo
