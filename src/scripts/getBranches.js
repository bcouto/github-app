import axios from "axios";

const getBranchMaster = async project => {
  const projectBranchesURI =
    "https://api.github.com/repos/marvin-ai/" + project.name + "/branches";

  const response = await axios.get(projectBranchesURI);
  return response.data;
};

export default getBranchMaster;
