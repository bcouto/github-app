import axios from "axios";

const getCommitsInfo = async commitsURI => {
  const response = await axios.get(commitsURI);
  return response.data;
};

export default getCommitsInfo;
