import axios from "axios";

export default async () => {
  const repositoryURI = "https://api.github.com/users/marvin-ai/repos";
  const response = await axios.get(repositoryURI);
  return response.data;
};
